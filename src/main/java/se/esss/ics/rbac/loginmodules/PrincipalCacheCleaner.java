/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.loginmodules;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.jboss.security.SecurityContextAssociation;

/**
 * This clears the JBoss principal cache when the session is destroyed.
 *
 * JBoss by default caches permissions and roles even after session is destroyed. There is an option to change this
 * behaviour in configuration, but is broken in JBoss 8 (https://issues.jboss.org/browse/WFLY-3221).
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class PrincipalCacheCleaner implements HttpSessionBindingListener {

    private static final Logger LOGGER = Logger.getLogger(PrincipalCacheCleaner.class.getName());

    private String name;
    private String securityDomain;

    /**
     * Constructs an instance that works for the provided principal.
     *
     * @param name
     *            the principal name
     */
    public PrincipalCacheCleaner(String name) {
        super();
        this.name = name;
    }

    @Override
    public void valueBound(HttpSessionBindingEvent event) {
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Binding principal.");
            LOGGER.log(Level.FINE, "Principal: " + name);
        }
        securityDomain = SecurityContextAssociation.getSecurityContext().getSecurityDomain();
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Security domain: " + securityDomain);
        }
    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Unbinding principal.");
        }

        /*
         * Under some conditions it can happen that the security domain does not exist. In such a case the cache does
         * not need to be cleared.
         */
        if (securityDomain != null) {
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "Clearing cache for principal:" + name);
            }
            try {
                ObjectName jaasMgr = new ObjectName("jboss.as:subsystem=security,security-domain=" + securityDomain);
                Object[] params = { name };
                String[] signature = { "java.lang.String" };
                MBeanServer server = (MBeanServer) MBeanServerFactory.findMBeanServer(null).get(0);
                server.invoke(jaasMgr, "flushCache", params, signature);

            } catch (MalformedObjectNameException | InstanceNotFoundException | ReflectionException
                    | MBeanException e) {
                if (LOGGER.isLoggable(Level.SEVERE)) {
                    LOGGER.log(Level.SEVERE, "Clearing cache failed", e);
                }
            }
        }
    }

    /**
     * @return the principal name
     */
    public String getName() {
        return name;
    }

}
