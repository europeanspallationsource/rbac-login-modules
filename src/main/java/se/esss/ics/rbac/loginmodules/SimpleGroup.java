/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.loginmodules;

import java.security.Principal;
import java.security.acl.Group;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Simple implementation of the {@link Group} interface
 * 
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
public class SimpleGroup implements Group {
    private final String name;
    private Map<String, Principal> members = new HashMap<String, Principal>();

    /**
     * Constructs a group with a name.
     * 
     * @param name
     *            the name of a group.
     */
    public SimpleGroup(String name) {
        super();
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean addMember(Principal principal) {
        final boolean isAlreadyMember = members.containsKey(principal.getName());
        if (!isAlreadyMember)
            members.put(principal.getName(), principal);
        return isAlreadyMember;
    }

    @Override
    public boolean isMember(Principal principal) {
        return members.containsKey(principal.getName());
    }

    @Override
    public Enumeration<Principal> members() {
        return Collections.enumeration(members.values());
    }

    @Override
    public boolean removeMember(Principal principal) {
        return members.remove(principal.getName()) != null;
    }

}
