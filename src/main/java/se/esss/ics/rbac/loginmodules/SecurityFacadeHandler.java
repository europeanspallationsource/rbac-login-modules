/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.loginmodules;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.jacc.PolicyContext;
import javax.security.jacc.PolicyContextException;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityFacade;

/**
 * This associates a new {@link ISecurityFacade} to the HTTP session when it is created. Each session has a separate
 * instance of SecurityFacade which exists for the duration of the session, then is destroyed.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@WebListener
public class SecurityFacadeHandler implements HttpSessionListener {

    private static final Logger LOGGER = Logger.getLogger(SecurityFacadeHandler.class.getName());

    private static final String SECURITY_FACADE = ISecurityFacade.class.getName();

    /**
     * Returns a servlet request that is being handled. This method should only be invoked if such request currently
     * exists.
     *
     * @return the servlet request
     */
    static HttpServletRequest getServletRequest() {
        try {
            return (HttpServletRequest) PolicyContext.getContext(HttpServletRequest.class.getName());
        } catch (PolicyContextException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns the current session. This method should only be invoked if the session currently exists.
     *
     * @return the current session
     */
    static HttpSession getSession() {
        HttpServletRequest request = getServletRequest();
        return request == null ? null : request.getSession();
    }

    /**
     * Returns the security facade associated with the current session. This method should only be invoked if the
     * session currently exists.
     *
     * @return the security facade
     */
    static ISecurityFacade getSecurityFacade() {
        HttpSession session = getSession();
        if (session==null) {
            return null;
        }
        final Object sessionBinder = session.getAttribute(SECURITY_FACADE);
        if (sessionBinder==null || !(sessionBinder instanceof SecurityFacadeBinder)) {
            return null;
        }
        return ((SecurityFacadeBinder) sessionBinder).getSecurityFacade();
    }

    /**
     * Returns the set of permissions that are granted to the logged in user in the context of the resources
     * that uses that {@link SecurityFacade}.
     *
     * @return the set of granted permissions or null if not set yet
     */
    static Set<String> getGrantedPermissions() {
        HttpSession session = getSession();
        return session == null ? new HashSet<String>(0) :
            ((SecurityFacadeBinder) session.getAttribute(SECURITY_FACADE)).getGrantedPermissions();
    }

    /**
     * Sets the granted permissions for the logged in user.
     *
     * @param permissions the names of granted permissions
     */
    static void setGrantedPermissions(Set<String> permissions) {
        HttpSession session = getSession();
        if (session != null) {
            ((SecurityFacadeBinder) session.getAttribute(SECURITY_FACADE)).setGrantedPermissions(permissions);
        }
    }

    @Override
    public void sessionCreated(HttpSessionEvent evt) {
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Session created.");
        }

        final HttpSession httpSession = evt.getSession();
        if (httpSession != null) {
            httpSession.setAttribute(SECURITY_FACADE, new SecurityFacadeBinder()); // NOSONAR: with this design we do not
        }
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent evt) {
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Session destroyed.");
        }

        /*
         * Since the HTTP session attributes are already cleared when this method is invoked, the security facade is
         * destroyed in the SecurityFacadeBinder.
         */
    }
}
