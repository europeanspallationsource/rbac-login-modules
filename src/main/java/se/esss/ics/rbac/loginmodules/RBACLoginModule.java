/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.loginmodules;

import java.io.IOException;
import java.security.Principal;
import java.security.acl.Group;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import se.esss.ics.rbac.access.AccessDeniedException;
import se.esss.ics.rbac.access.AuthenticationFailedException;
import se.esss.ics.rbac.access.Credentials;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityCallback;
import se.esss.ics.rbac.access.SecurityCallbackAdapter;
import se.esss.ics.rbac.access.SecurityFacadeException;
import se.esss.ics.rbac.access.Token;
import se.esss.ics.rbac.httputil.RBACCookie;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSet.Builder;

/**
 * Login Module that uses DISCS RBAC
 *
 * based on org.jboss.security.auth.spi.AbstractServerModule
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 *
 */
public class RBACLoginModule extends SecurityCallbackAdapter implements SecurityCallback, LoginModule { // NOSONAR:
    // coupling: exceeds the limit of using 20 other classes due to tying of java and RBAC security implementations

    private static final Logger LOGGER = Logger.getLogger(RBACLoginModule.class.getName());

    private static final String CALLER_PRINCIPAL = "CallerPrincipal";

    private static final String RBAC_RESOURCE = "RBACResource";
    private static final String PERMISSIONS_OF_INTEREST = "RBACPermissionsOfInterest";
    private static final String PREFERRED_ROLE = "RBACPreferredRole";

    private static final String AS_ROLES = "loggedInASRoles";
    private static final String MAP_PERMISSIONS = "mapRBACPermissionsToASRoles";

    private Subject subject;
    private CallbackHandler callbackHandler;
    private boolean loginOk = false;

    /*
     * Configuration fields
     */
    private String rbacResource;
    private String preferredRole;
    private Set<String> asRoles;
    private Set<String> permissionsOfInterest;
    private boolean mapRoles = false;

    /*
     * Parameters set fields login
     */
    private String username;
    private char[] password;
    private Token rbacToken;
    private Set<String> grantedPermissions;

    @Override
    public void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState,
            Map<String, ?> options) {
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Initializing.");
        }

        this.subject = subject;
        this.callbackHandler = callbackHandler;

        rbacResource = (String) options.get(RBAC_RESOURCE);
        preferredRole = (String) options.get(PREFERRED_ROLE);

        asRoles = parseCommaSeparated((String) options.get(AS_ROLES));
        permissionsOfInterest = parseCommaSeparated((String) options.get(PERMISSIONS_OF_INTEREST));
        mapRoles = Boolean.parseBoolean((String) options.get(MAP_PERMISSIONS));
    }

    @Override
    public boolean login() throws LoginException {
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Invoking login.");
        }
        getUsernameAndPassword();

        loginOk = false;
        rbacToken = null;
        try {
            final ISecurityFacade securityFacade = SecurityFacadeHandler.getSecurityFacade();
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "Security Facade instance used: " + securityFacade);
            }
            if (securityFacade == null) {
                throw new LoginException(RBACLoginModule.class.getName()
                        + " Login failed, cannot find security facade!");
            }

            boolean checkPermissions = false;
            try {
                rbacToken = securityFacade.getToken();
            } catch (SecurityFacadeException e) {
                // ignore token invalid
            }
            login: {
                if (RBACCookie.NAME.equals(username)) {
                    if (rbacToken != null) {
                        if (Arrays.equals(rbacToken.getTokenID(), password)) {
                            if (LOGGER.isLoggable(Level.FINE)) {
                                LOGGER.log(Level.FINE, "Already logged in.");
                            }
                            break login;
                        }
                        securityFacade.logout();
                    }
                    securityFacade.setToken(password);
                    if (securityFacade.isTokenValid()) {
                        if (LOGGER.isLoggable(Level.FINE)) {
                            LOGGER.log(Level.FINE, "Token cookie is valid.");
                        }
                    } else {
                        if (LOGGER.isLoggable(Level.FINE)) {
                            LOGGER.log(Level.FINE, "Cookie token no longer exists.");
                        }
                        rbacToken = null;
                        SecurityFacadeHandler.setGrantedPermissions(null);
                        return false;
                    }
                    rbacToken = securityFacade.getToken();
                    checkPermissions = true;
                } else {
                    if (rbacToken != null && securityFacade.isTokenValid()) {
                        if (LOGGER.isLoggable(Level.FINE)) {
                            LOGGER.log(Level.FINE, "Token already exists.");
                        }
                    } else {
                        securityFacade.setDefaultSecurityCallback(this);
                        rbacToken = securityFacade.authenticate();
                        securityFacade.setDefaultSecurityCallback(null);
                        checkPermissions = true;
                    }
                }
            }

            if (rbacToken == null) {
                throw new LoginException(RBACLoginModule.class.getName() + " Login failed, invalid credentials!");
            }

            Set<String> permissions = SecurityFacadeHandler.getGrantedPermissions();
            if (permissions == null || checkPermissions) {
                final Builder<String> grantedPermissionsBuilder = ImmutableSet.builder();
                if (rbacResource != null && !rbacResource.isEmpty() && !permissionsOfInterest.isEmpty()) {
                    final String[] permissionsArray = permissionsOfInterest
                            .toArray(new String[permissionsOfInterest.size()]);
                    final Map<String, Boolean> permissionsMap = securityFacade.hasPermissions(rbacResource,
                            permissionsArray);

                    for (Map.Entry<String, Boolean> permissionOfInterest : permissionsMap.entrySet()) {
                        if (permissionOfInterest.getValue()) {
                            grantedPermissionsBuilder.add(permissionOfInterest.getKey());
                        }
                    }
                }
                grantedPermissions = grantedPermissionsBuilder.build();
                SecurityFacadeHandler.setGrantedPermissions(grantedPermissions);
            } else {
                grantedPermissions = ImmutableSet.copyOf(permissions);
            }
        } catch (AuthenticationFailedException | AccessDeniedException e) {
            SecurityFacadeHandler.setGrantedPermissions(null);
            // incorrect username or password or expired token
            throw new LoginException(e.getMessage());
        } catch (SecurityFacadeException e) {
            SecurityFacadeHandler.setGrantedPermissions(null);
            if (LOGGER.isLoggable(Level.SEVERE)) {
                LOGGER.log(Level.SEVERE, "Login failed with error.", e);
            }
            throw new LoginException(RBACLoginModule.class.getName() + " Login failed with error: " + e.getMessage());
        }

        loginOk = true;

        return true;
    }

    @Override
    public boolean abort() throws LoginException {
        return true;
    }

    @Override
    public boolean commit() throws LoginException {
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Invoking commit.");
        }
        if (!loginOk) {
            return false;
        }
        final Set<Principal> principals = subject.getPrincipals();
        final Principal identity = getIdentity();
        principals.add(identity);

        Group[] roleSets = getRoleSets();
        for (int g = 0; g < roleSets.length; g++) {
            final Group group = roleSets[g];
            final String name = group.getName();
            final Group subjectGroup = createGroup(name, principals);

            // Copy the group members to the Subject group
            final Enumeration<? extends Principal> members = group.members();
            while (members.hasMoreElements()) {
                Principal role = (Principal) members.nextElement();
                subjectGroup.addMember(role);
            }
        }
        // add the CallerPrincipal group if none has been added in getRoleSets
        Group callerGroup = getCallerPrincipalGroup(principals);
        if (callerGroup == null) {
            callerGroup = new SimpleGroup(CALLER_PRINCIPAL);
            callerGroup.addMember(identity);
            principals.add(callerGroup);
        }

        HttpSession session = SecurityFacadeHandler.getSession();
        if (session == null) {
            return false;
        }
        PrincipalCacheCleaner currentCleaner = (PrincipalCacheCleaner) session.getAttribute(PrincipalCacheCleaner.class
                .getName());

        /*
         * Since login is in certain cases called multiple times for the same user, only replace cleaner if the user has
         * changed.
         */
        if (currentCleaner == null || !currentCleaner.getName().equals(identity.getName())) {
            session.setAttribute(PrincipalCacheCleaner.class.getName(), new PrincipalCacheCleaner(identity.getName()));
        }

        return true;
    }

    /**
     * Helper method used in commit to find the CallerPrincipal
     *
     * @param principals
     * @return
     */
    private Group getCallerPrincipalGroup(Set<Principal> principals) {
        for (Principal principal : principals) {
            if (principal instanceof Group) {
                final Group group = Group.class.cast(principal);
                if (CALLER_PRINCIPAL.equals(group.getName())) {
                    return group;
                }
            }
        }
        return null;
    }

    /**
     * Returns an existing group or creates new is one not found
     *
     * @param name
     * @param principals
     * @return
     */
    private Group createGroup(String name, Set<Principal> principals) {
        Group roles = null;
        final Iterator<Principal> iter = principals.iterator();
        while (iter.hasNext()) {
            Object next = iter.next();
            if (next instanceof Group) {
                Group grp = (Group) next;
                if (grp.getName().equals(name)) {
                    roles = grp;
                    break;
                }
            }
        }
        // If we did not find a group create one
        if (roles == null) {
            roles = new SimpleGroup(name);
            principals.add(roles);
        }
        return roles;
    }

    @Override
    public boolean logout() throws LoginException {
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Invoking logout.");
        }

        boolean result = false;

        try {
            final ISecurityFacade securityFacade = SecurityFacadeHandler.getSecurityFacade();
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "Security Facade instance used: " + securityFacade);
            }
            if (securityFacade == null) {
                if (LOGGER.isLoggable(Level.SEVERE)) {
                    LOGGER.log(Level.SEVERE, "Failed to logout from RBAC. No security facade available.");
                }
                throw new LoginException(RBACLoginModule.class.getName()
                        + "Failed to logout from RBAC: no security facade available.");
            } else {
                if (securityFacade.getLocalToken() != null && securityFacade.isTokenValid()) {
                    result = securityFacade.logout();
                } else {
                    result = true;
                }
                SecurityFacadeHandler.setGrantedPermissions(null);
            }
        } catch (Exception e) {
            if (LOGGER.isLoggable(Level.SEVERE)) {
                LOGGER.log(Level.SEVERE, "Failed to logout from RBAC.", e);
            }
            throw new LoginException(RBACLoginModule.class.getName() + "Failed to logout from RBAC: " + e.getMessage());
        }

        username = null;
        password = null;
        rbacToken = null;
        grantedPermissions = null;
        loginOk = false;

        return result;
    }

    /**
     * Used in the commit method to insert our identity
     *
     * @return
     */
    private Principal getIdentity() {
        return new RBACPrincipal(rbacToken.getUsername(), rbacToken.getFirstName(), rbacToken.getLastName(),
                rbacResource, rbacToken.getTokenID(), ImmutableSet.copyOf(rbacToken.getRoles()), grantedPermissions);
    }

    /**
     * Used in the commit method to insert our roles and CallerPrincipal
     *
     * @return
     * @throws LoginException
     */
    private Group[] getRoleSets() throws LoginException {
        final SimpleGroup roles = new SimpleGroup("Roles");

        final Collection<String> inputRoles = mapRoles ? grantedPermissions : asRoles;

        for (String role : inputRoles)
            roles.addMember(new SimplePrincipal(role));

        final SimpleGroup callerPrincipal = new SimpleGroup(CALLER_PRINCIPAL);
        callerPrincipal.addMember(getIdentity());

        return new Group[] { roles, callerPrincipal };
    }

    /**
     * Fills out username and password members with obtained username and password
     *
     * @throws LoginException
     */
    private void getUsernameAndPassword() throws LoginException {
        // prompt for a username and password
        if (callbackHandler == null) {
            throw new LoginException(RBACLoginModule.class.getName() + " Error while getting username and password");
        }

        final NameCallback nc = new NameCallback("username", "guest");
        final PasswordCallback pc = new PasswordCallback("password", false);
        final Callback[] callbacks = { nc, pc };

        try {
            callbackHandler.handle(callbacks);
            username = nc.getName();
            char[] tmpPassword = pc.getPassword();
            if (tmpPassword != null) {
                password = new char[tmpPassword.length];
                System.arraycopy(tmpPassword, 0, password, 0, tmpPassword.length);
                pc.clearPassword();
            }
        } catch (IOException | UnsupportedCallbackException e) {
            if (LOGGER.isLoggable(Level.SEVERE)) {
                LOGGER.log(Level.SEVERE, "Failed to get credentials.", e);
            }
            throw new LoginException(RBACLoginModule.class.getName() + "Failed to get credentials: " + e.getMessage());
        }
    }

    /**
     * Parses comma separated roleNames optionally passed as login module argument "roles"
     *
     * If not specified will return empty-set
     *
     * @param roleNames
     * @return
     */
    private Set<String> parseCommaSeparated(String roleNames) {
        if (roleNames != null) {
            return ImmutableSet.copyOf(roleNames.split("[,]"));
        } else {
            return ImmutableSet.of();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see se.esss.ics.rbac.access.SecurityCallbackAdapter#getCredentials()
     */
    @Override
    public Credentials getCredentials() {
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Providing credentials.");
        }

        final HttpServletRequest httpServletRequest = SecurityFacadeHandler.getServletRequest();
        if (httpServletRequest == null) {
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "Credentials unknown. No servlet request available.");
            }
            return null;
        } else {
            final String ip = httpServletRequest.getRemoteAddr();
            final int port = httpServletRequest.getRemotePort();
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "Credentials: User: " + username + " Role: " + preferredRole + " IP: " + ip
                    + " Port: " + port);
            }

            return new Credentials(username, password, preferredRole, ip);
        }
    }
}
