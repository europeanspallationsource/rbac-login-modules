/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.loginmodules;

import java.security.Principal;
import java.util.Collection;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.security.auth.login.LoginException;

import se.esss.ics.rbac.access.AccessDeniedException;
import se.esss.ics.rbac.access.ISecurityFacade;
import se.esss.ics.rbac.access.SecurityFacadeException;

import com.google.common.collect.ImmutableSet;

/**
 * A {@link Principal} representing a RBAC user. Information provided by an instance of this class is only valid for the
 * duration of the session.
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
public class RBACPrincipal implements Principal {

    private static final Logger LOGGER = Logger.getLogger(RBACPrincipal.class.getName());

    private static final String BASIC_PERMISSION_ERROR = " Error accessing permission data. ";

    private final String firstName;
    private final String lastName;
    private final String username;
    private final String resource;
    private final char[] tokenID;
    private final ImmutableSet<String> roles;
    private final ImmutableSet<String> permissions;

    /**
     * Constructs RBAC principle.
     *
     * @param username the user name
     * @param firstName first name of the user
     * @param lastName last name of the user
     * @param resource the resource associated with this principal
     * @param tokenID the token ID received during authentication
     * @param roles the roles of this principal
     * @param permissions the permissions of this principal
     */
    public RBACPrincipal(String username, String firstName, String lastName, String resource, char[] tokenID,
            Collection<String> roles, Collection<String> permissions) {
        this.username = username;
        this.resource = resource;
        this.firstName = firstName;
        this.lastName = lastName;

        this.roles = ImmutableSet.copyOf(roles);
        this.permissions = ImmutableSet.copyOf(permissions);
        this.tokenID = tokenID;
    }

    /**
     * @return the token ID
     */
    public char[] getTokenID() {
        return tokenID;
    }

    /**
     * @return the first name of the user
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @return the last name of the user
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @return the username of the owner
     */
    public String getUsername() {
        return username;
    }

    /** @return the resource associated with this principal */
    public String getResource() {
        return resource;
    }

    /** @return the roles of this principal */
    public Set<String> getRoles() {
        return roles;
    }

    /**
     * Returns the permissions of this principal among permissions of interest specified in the login module
     * configuration.
     *
     * @return the set of permissions
     */
    public Set<String> getPermissions() {
        return permissions;
    }

    /**
     * Checks if the user is logged in and returns true if it or false if it is not.
     *
     * @return true if the user is logged in or false otherwise
     */
    public boolean isLoggedIn() {
        try {
            ISecurityFacade facade = SecurityFacadeHandler.getSecurityFacade();
            if (facade == null) {
                return false;
            } else {
                return facade.isTokenValid();
            }
        } catch (SecurityFacadeException e) {
            return false;
        }
    }

    /**
     * Checks whether the definition of the given permission exists.
     *
     * @param permission the permission to check
     * @return true if permission exists, else false
     * @throws LoginException if there was an error when trying to check for the permission.
     */
    public boolean runtimePermissionExists(String permission) throws LoginException {
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Invoking permission exists check.");
        }

        try {
            ISecurityFacade facade = SecurityFacadeHandler.getSecurityFacade();
            if (facade == null) {
                throw new LoginException(RBACLoginModule.class.getName() + BASIC_PERMISSION_ERROR
                        + "Failed to get a valid security facade.");
            } else {
                facade.hasPermission(resource, permission);
                if (LOGGER.isLoggable(Level.FINE)) {
                    LOGGER.log(Level.FINE, "Permission '" + permission + "' exists.");
                }
                return true;
            }
        } catch (AccessDeniedException e) {
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "Access denied.", e);
            }
            // if token has timed out, return most restrictive state
            return true;
        } catch (SecurityFacadeException e) {
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "Permission not found.", e);
            }
            if (e.getResponseCode() == 404) {
                return false;
            }
            throw new LoginException(RBACLoginModule.class.getName() + BASIC_PERMISSION_ERROR + e.getMessage());
        } catch (IllegalArgumentException e) {
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, e.getClass().getName(), e);
            }
            throw new LoginException(RBACLoginModule.class.getName() + BASIC_PERMISSION_ERROR + e.getMessage());
        }
    }

    /**
     * Specifies if this principal has the specified permission. The permission can be any permission, not just
     * permissions of interest defined in the login module configuration.
     *
     * @param permission the permission to check
     * @return true if this principal has permission, else false
     * @throws LoginException if there was an error when trying to check for the permission, or the permission does not
     *         exist
     */
    public boolean hasRuntimePermission(String permission) throws LoginException {

        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Invoking permission check.");
        }

        ISecurityFacade facade = SecurityFacadeHandler.getSecurityFacade();
        if (facade == null) {
            if (LOGGER.isLoggable(Level.SEVERE)) {
                LOGGER.log(Level.SEVERE, "Failed to get a valid security facade.");
            }
            throw new LoginException(RBACLoginModule.class.getName() + BASIC_PERMISSION_ERROR
                    + "Failed to get a valid security facade.");
        } else {
            try {
                final boolean hasPermission = facade.hasPermission(resource, permission);
                if (LOGGER.isLoggable(Level.FINE)) {
                    LOGGER.log(Level.FINE, "Principal '" + username + (hasPermission ? " has" : " does not have")
                        + " permission '" + permission + "'.");
                }
                return hasPermission;

            } catch (AccessDeniedException e) {
                if (LOGGER.isLoggable(Level.FINE)) {
                    LOGGER.log(Level.FINE, "Access denied.", e);
                }
                // if token has timed out, return most restrictive state
                return false;
            } catch (SecurityFacadeException | IllegalArgumentException e) {
                if (LOGGER.isLoggable(Level.SEVERE)) {
                    LOGGER.log(Level.SEVERE, "Failed to get permission.", e);
                }
                throw new LoginException(RBACLoginModule.class.getName() + BASIC_PERMISSION_ERROR + e.getMessage());
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see java.security.Principal#getName()
     */
    @Override
    public String getName() {
        return username;
    }
}
