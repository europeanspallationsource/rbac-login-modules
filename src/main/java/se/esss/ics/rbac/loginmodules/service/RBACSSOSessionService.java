/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of RBAC.
 * RBAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.rbac.loginmodules.service;

import java.io.Serializable;
import java.lang.management.ManagementFactory;
import java.security.Principal;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.security.auth.login.LoginException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.security.SecurityContextAssociation;

import se.esss.ics.rbac.httputil.CookieUtilities;
import se.esss.ics.rbac.httputil.RBACCookie;
import se.esss.ics.rbac.loginmodules.RBACPrincipal;

/**
 *
 * <code>RBACSSOSessionService</code> is the service that manages authentication and authorisation in a web application
 * supported by jaas. The service provides facilities to login or logout the user as well as methods to check
 * permissions. The info about the logged in user can also be retrieved.
 *
 * @author <a href="mailto:jaka.bobnar@cosylab.com">Jaka Bobnar</a>
 */
@RequestScoped
public class RBACSSOSessionService implements Serializable {

    private static final long serialVersionUID = 1743995702242148849L;

    private static final Logger LOGGER = Logger.getLogger(RBACSSOSessionService.class.getName());

    private Boolean loggedIn;

    @Inject
    private HttpServletRequest request;

    /**
     * Returns the request associated with this service. This is the currently active request, which is obtained from
     * the external context.
     *
     * @return the request associated with this service
     */
    public HttpServletRequest getRequest() {
        if (request == null) {
            return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        } else {
            return request;
        }
    }

    private HttpServletResponse getResponse() {
        try {
            return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Returns the distinguished name of the logged in user. If no user is logged in, null is returned.
     *
     * @return the username of the logged in user
     */
    public String getLoggedInName() {
        final RBACPrincipal rbacPrincipal = castPrincipalFromRequest();
        return rbacPrincipal == null ? null : rbacPrincipal.getName();
    }

    /**
     * Returns the name of the logged in user, which should be used for all display purposes. In this case this should
     * be the user's first name. If the user is not logged in, null is returned.
     *
     * @return the visible name of the logged in user
     */
    public String getVisibleName() {
        final RBACPrincipal rbacPrincipal = castPrincipalFromRequest();
        return rbacPrincipal == null ? null : rbacPrincipal.getFirstName();
    }

    /**
     * @return true if the user is logged in
     */
    public boolean isLoggedIn() {
        final RBACPrincipal principal = castPrincipalFromRequest();
        if (principal == null) {
            flushCache();
            return false;
        }
        if (loggedIn == null) {
            loggedIn = Boolean.valueOf(principal.isLoggedIn());
            if (!loggedIn) {
                logout();
            }
        }
        return loggedIn == null ? false : loggedIn;

    }

    /**
     * Process the RBAC cookie.
     */
    @PostConstruct
    protected void checkCookie() {
        if (CookieUtilities.isUseSingleSignOn()) {
            HttpServletRequest request = getRequest();
            if (request != null) {
                Cookie[] allCookies = request.getCookies();
                for (Cookie c : allCookies) {
                    if (RBACCookie.NAME.equals(c.getName())) {
                        try {
                            String cookieVal = c.getValue();
                            if (RBACCookie.LOGGED_OUT_VALUE.equals(cookieVal)) {
                                clearLoggedIn();
                                return;
                            } else {
                                RBACPrincipal principal = castPrincipalFromRequest();
                                char[] token = CookieUtilities.decode(cookieVal);
                                if (principal == null || Arrays.equals(token, principal.getTokenID())) {
                                    login(RBACCookie.NAME, new String(token));
                                }
                                return;
                            }
                        } catch (Exception e) {
                            LOGGER.log(Level.FINE, "Cookie found but token could not be used.", e);
                            HttpServletResponse response = getResponse();
                            if (response != null) {
                                response.addCookie(new RBACCookie());
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Check if the logged in user has permission. The method first checks if the user is still logged in and if yes, it
     * returns the cached permission.
     *
     * @param permission the permission name to check
     * @return true if the logged in user has permission or false otherwise
     */
    public boolean hasPermission(String permission) {
        if (!isLoggedIn()) {
            return false;
        }

        final RBACPrincipal rbacPrincipal = castPrincipalFromRequest();
        return rbacPrincipal.getPermissions().contains(permission);
    }

    /**
     * Checks if the logged in user has permission. In contrast to {@link #hasPermission(String)} this method will
     * always ask the central service to check the permission.
     *
     * @param permission the permission to check
     * @return true if the permission is granted or false otherwise
     * @throws LoginException if there was an error checking permission
     */
    public boolean hasRuntimePermission(String permission) throws LoginException {
        if (!isLoggedIn()) {
            return false;
        }

        final RBACPrincipal rbacPrincipal = castPrincipalFromRequest();
        return rbacPrincipal.hasRuntimePermission(permission);
    }

    /**
     * Logs in with the given credentials. Whether the action succeeded is stored in the returned result.
     *
     * @param username the user name to use
     * @param password the password
     * @return the result with the message and successful flag
     */
    public Message login(String username, String password) {
        try {
            clearLoggedIn();
            if (getRequest().getUserPrincipal() == null) {
                getRequest().login(username, password);
            }

            if (CookieUtilities.isUseSingleSignOn()) {
                HttpServletResponse response = getResponse();
                if (response != null) {
                    char[] tokenId = castPrincipalFromRequest().getTokenID();
                    response.addCookie(new RBACCookie(CookieUtilities.encode(tokenId)));
                }
            }
            return new Message("Sign in successful.", true);
        } catch (ServletException e) {
            return new Message(e.getMessage(), false);
        }
    }

    /**
     * Logs out the currently logged in user and returns the result as the message. If the logout was successful, the
     * message is set as successful, if it failed, the successful flag is set to false.
     *
     * @return the result
     */
    public Message logout() {
        return logout(true);
    }

    private Message logout(boolean invalidateSession) {
        try {
            flushCache();
            clearLoggedIn();
            if (getRequest().getUserPrincipal() != null) {
                getRequest().logout();
            }

            if (CookieUtilities.isUseSingleSignOn()) {
                HttpServletResponse response = getResponse();
                if (response != null) {
                    response.addCookie(new RBACCookie());
                }
            }
            if (invalidateSession) {
                getRequest().getSession().invalidate();
            }

            return new Message("Sign out successful.", true);
        } catch (ServletException | IllegalStateException e) {
            return new Message(e.getMessage(), false);
        }
    }

    /**
     * Flush the security domain cache. This is a workaround for jboss. If the cache is not flushed the logout might not
     * call the jaas login module.
     */
    protected void flushCache() {
        try {
            MBeanServerConnection mbeanServerConnection = ManagementFactory.getPlatformMBeanServer();
            String security = SecurityContextAssociation.getSecurityContext().getSecurityDomain();
            ObjectName mbeanName = new ObjectName("jboss.as:subsystem=security,security-domain=" + security);
            mbeanServerConnection.invoke(mbeanName, "flushCache", null, null);
        } catch (Exception e) {
            // ignore
        }
    }

    /**
     * Clears the loggedIn flag, which is the cache for storing the logged in state obtained from the authentication
     * server. The logged in state is initialised once per request and cached for the duration of the request. If this
     * service is used in a scope that is wider than request (e.g. SessionScope), than the loggedIn flag might needs to
     * be reset.
     */
    protected void clearLoggedIn() {
        loggedIn = null;
    }

    protected RBACPrincipal castPrincipal(Principal principal) {
        // Casting fails when the RBACPrincipal was loaded by other class loader (e.g. from previous deployment)
        // This check prevents exceptions in this case and forces things to reload
        return (principal instanceof RBACPrincipal) ? (RBACPrincipal) principal : null;
    }

    protected RBACPrincipal castPrincipalFromRequest() {
        return castPrincipal(getRequest().getUserPrincipal());
    }
}
